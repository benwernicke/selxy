#include <belt/flag.h>
#include <belt/holy.h>
#include <belt/panic.h>
#include <belt/todo.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Tuple Tuple;
struct Tuple {
    u32 x;
    u32 y;
};

#include "vec.h"

bool help_flag = 0;
Str input_file_flag = { };
bool keep_orient_flag = 1;

constexpr u32 MATCH_ALL = ((u32)-1);
#define MATCH_ERROR ((u32)-2)

static u32 to_u32(const char* s, u32 len) {
    if (len == 1 && s[0] == '*') {
        return MATCH_ALL;
    }
    u32 sum = 0;
    for (u32 i = 0; i < len; ++i) {
        if (!('0' <= s[i] && s[i] <= '9')) return MATCH_ERROR;
        sum *= 10;
        sum += s[i] - '0';
    }
    return sum;
}

static bool coords_match(Vec(Tuple)* coords, Tuple b) {
    for (u32 i = 0; i < coords->len; ++i) {
        const Tuple a = coords->buf[i];
        if ((a.y == b.y || a.y == MATCH_ALL) && (a.x == b.x || a.x == MATCH_ALL)) {
            return 1; 
        }
    }
    return 0;
}

static void from_file(FILE* input, Vec(Tuple)* coords) {
    auto word = vec_create(u8, .cap = 32);

    Tuple curr = { };
    char c = 0;
    while (c != EOF) {
        for (c = fgetc(input); isspace(c); c = (u8)fgetc(input)) { 
            if (c == '\n') {
                curr.y += 1;
                curr.x = 0;
            }
        }

        for (; !isspace(c) && c != EOF && c != '\0'; c = (u8)fgetc(input)) {
            vec_push(&word, (u8)c);
        }

        if (coords_match(coords, curr)) {
            printf("%.*s", word->len, word->buf);
            if (keep_orient_flag) {
                putc(c == '\n' ? '\n' : '\t', stdout);
            } else { 
                putc('\n', stdout);
            }
        }

        if (c == '\n') {
            curr.y += 1;
            curr.x = 0;
        } else {
            curr.x += 1;
        }
        word->len = 0;
    }
    free(word);
}

int main(int argc, char* argv[argc]) {

    /* flag stuff */
    {
        Flag flags[] = {
            MAKE_FLAG(&help_flag, "-h", "--help", "print this page and exit"),
            MAKE_FLAG(&input_file_flag, "-i", "--input-file", "use the given file as input source"),
            MAKE_FLAG(&keep_orient_flag, "-ko", "--keep-orientation", "keep the orientation of the elements"),
        };
        u32 flags_len = sizeof(flags) / sizeof(*flags);
        Flag_Error err = flag_parse(
                argc, argv, 
                flags_len, flags,
                .filtered = { &argc, &argv } );
        if (err) {
            fprintf(stderr, "Error with given flags: %s at %s\n",
                    flag_error_format(err),
                    argv[flag_error_at(err)]);
            exit(1);
        }
        if (help_flag) {
            char* general_info =    "selxy: program to select elements from a table\n"
                                    "by selecting certain x:y coordinates\n"
                                    "\n"
                                    "General usage: 'selxy <x:y> <x:y> ...'\n"
                                    "where 'x' or 'y' may be '*' to represent all numbers\n"
                                    "and you can give as many coordinate pairs as you wish\n";
            flag_print_help(stderr, general_info, flags_len, flags); 
            exit(0);
        }
    }

    auto coords = vec_create(Tuple);

    /* parse coordinates into vec*/
    {
        int err = vec_reserve(&coords, argc);
        PANIC_IF(err, "could not allocate enough space for: %d coordinates", argc);

        for (u32 i = 1; i < (u32)argc; ++i) {
            char* sep_ = strchr(argv[i], ':');
            PANIC_IF(!sep_, "could not interpret: '%s', missing seperator ':'", argv[i]);
            u32 sep = sep_ - argv[i];

            u32 len = strlen(argv[i]);

            Tuple t = {
                .x = to_u32(&argv[i][0], sep),
                .y = to_u32(&argv[i][sep + 1], len - sep - 1),
            };
            PANIC_IF(t.x == MATCH_ERROR, "x part of '%s' is not a number nor '*'", argv[i]);
            PANIC_IF(t.y == MATCH_ERROR, "y part of '%s' is not a number nor '*'", argv[i]);

            vec_push_unchecked(coords, t);
        }
    }

    if (input_file_flag.buf) {
        FILE* f = fopen(input_file_flag.buf, "r");
        PANIC_IF(!f, "could not open file: '%s': %s", input_file_flag.buf, strerror(errno));
        from_file(f, coords);
        fclose(f);
    } else {
        from_file(stdin, coords);
    }

    free(coords);

    return 0;
}
