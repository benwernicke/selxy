#include <belt/vec/generator.h>

int main(void) {
    char* types[] = {
        "Tuple",
        "u8",
    };
    u32 types_len = sizeof(types) / sizeof(*types);
    generate(types_len, types);
    return 0;
}
