all: gen
	gcc -Wall -Wextra -g -fsanitize=address,leak,undefined -std=c2x main.c

release: gen
	gcc -O2 -std=c2x -march=native -mtune=native -o selxy main.c

install: release
	cp selxy /usr/bin

uninstall:
	cp /usr/bin/selxy 

generator:
	gcc -std=c2x vec_generator.c -o vec_gen.out

gen: generator
	./vec_gen.out

clean:
	rm -f selxy *.out *.o vec.h
